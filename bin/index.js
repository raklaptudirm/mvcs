#!/usr/bin/env node

/*
 * mvcs
 * https://github.com/raklaptudirm/mvcs
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

"use strict"

const fs = require("fs")
const cp = require("child_process")
const crypto = require("crypto")
const E = require("./escapes.js")
const readline = require("readline")
const { getFiles } = require("./repo.js")

let args = {}
const cwd = process.cwd()

/*
 * Parse Arguments
 */

console.log(getFiles([]))
return

{
  let req = process.argv.slice(2)
  args.command = req.splice(0, 1)[0]
  args.subcommands = []
  args.flags = []

  let flag = false
  let flagVal = {
    name: "",
    args: [],
  }

  for (const i of req) {
    if (i.startsWith("-")) {
      if (flag) {
        args.flags.push(flagVal)
      }
      flagVal = {
        name: i,
        args: [],
      }
    } else {
      if (flag) {
        flagVal.args.push(i)
      } else {
        args.subcommands.push(i)
      }
    }
  }

  if (flag) {
    args.flags.push(flagVal)
  }
}
/*
 */

;(async function () {
  /*
   * Global functions
   * [START]
   */
  const is = (string, regexp) => {
    let match = regexp.exec(string)
    if (string === "") return false
    if (match && match[0] === string) return true
    return false
  }

  function isRepo() {
    if (!fs.existsSync(".mvcs") || !fs.lstatSync(".mvcs").isDirectory()) {
      console.log(
        "The current directory is not a MVCS repository.\nUse `msvc init` to initaialize it."
      )
      return false
    }
    return true
  }

  function prompt(query) {
    return new Promise((resolve, reject) => {
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
      })

      rl.question(query, answer => {
        rl.close()
        resolve(answer)
      })
    })
  }

  function log(query) {
    process.stdout.write(query)
  }

  /*
   * List all files except ones in .mvcs dir
   */
  function list(dir, main) {
    let files = []
    fs.readdirSync(dir).forEach(file => {
      if (file === ".mvcs" && dir === main) return
      if (fs.lstatSync(`${dir}\\${file}`).isFile()) files.push(file)
      else
        list(`${dir}\\${file}`, dir).forEach(item => {
          files.push(`${file}\\${item}`)
        })
    })
    return files
  }

  const parse = {
    break: function (file) {
      return file.replace(/\r/g, "").split("\n")
    },
    /*
     * Return tree structure:
     * {
     *   name: String,
     *   hash: String
     * }
     */
    tree: function (tree) {
      const struct = []
      this.break(tree).forEach(item =>
        struct.push({
          name: item.slice(0, item.length - 65),
          hash: item.slice(item.length - 64),
        })
      )
      return struct
    },
    /*
     * [String,]
     */
    ignore: function parseIgnore(file) {
      return this.break(file).filter(item => item.trim() !== "")
    },
    commit: function (commit) {
      commit = this.break(commit)
      return {
        tree: commit[0],
        parent: commit[1],
        date: commit[2],
        name: commit[3],
        description: commit[4],
        author: commit[5],
      }
    },
  }

  const files = {
    read: function (path) {
      return fs.readFileSync(path).toString()
    },
    make: function (name, init) {
      name = name.replace(/\\/g, "/")
      if (!fs.existsSync(name)) {
        if (name.includes("/")) {
          const dir = name.slice(0, name.indexOf("/"))
          fs.mkdirSync(dir, { recursive: true })
        }
        fs.writeFileSync(name, init)
      }
    },
    dir: function (name) {
      if (!fs.existsSync(name)) fs.mkdirSync(name)
    },
    /*
     * previous -> tree_struct [{name:String,hash:String},]
     * current ->
     */
    compare: function (previous, current) {
      const newFiles = []
      for (const i of previous) {
        for (const j in current) {
          if (current[j].name === i.name) {
            if (current[j].hash !== i.hash) newFiles.push(i.name)
            current.splice(j, 1)
            break
          }
        }
      }
      return newFiles.concat(current.map(item => item.name))
    },
  }

  const string = {
    encode: function (string) {
      return string
        .replace(/\\/g, "\\\\")
        .replace(/\n/g, "\\n")
        .replace(/\r/g, "\\r")
    },
    parse: function (string) {
      return string
        .replace(/\\r/, "\r")
        .replace(/\\n/g, "\n")
        .replace(/\\\\/g, "\\")
    },
  }

  function SHA_hash(data) {
    return crypto.createHash("sha256").update(data).digest("hex")
  }

  /*
   * filelist -> used to take only strings.
   * Now tree_structs
   */
  function getChanges(parent, fileList) {
    return parent === "null"
      ? fileList.map(item => item.name)
      : files.compare(
          parse.tree(files.read(`.mvcs/objects/trees/${parent.tree}`)),
          fileList.slice()
        )
  }

  function loadGlobals() {
    if (fs.existsSync(".mvcsignore"))
      ignore = parse.ignore(files.read(".mvcsignore"))

    _commit.hash = files.read(`.mvcs/branches/${branch}`)
    _commit.parent =
      _commit.hash === "null"
        ? "null"
        : parse.commit(files.read(`.mvcs/objects/commits/${_commit.hash}`))
  }

  function generateTree(fileList) {
    let length = fileList.length,
      count = 1
    log("Constructing tree: " + E.CURSOR.SAVE)
    fileList.forEach(item => {
      log(
        `${
          E.CURSOR.RESTORE + E.ERASE.END_FROM_CURSOR + E.CURSOR.RESTORE
        }${Math.floor((count / length) * 100)}% (${count}/${length})`
      )
      _tree.struct.push({ name: item, hash: SHA_hash(fs.readFileSync(item)) })
      count++
    })
    console.log(", done.")
  }

  function updateBranch(hash) {
    log(`Updating ${branch}`)
    fs.writeFileSync(`.mvcs/branches/${branch}`, hash)
    console.log(", done.")
  }

  /*
   * Get Credentials
   */

  if (!fs.existsSync(__dirname + "/../.credentials")) {
    const name = await prompt("Enter your commit name: ")

    const email = await prompt("Enter your commit email: ")

    console.log(`${name} <${email}>`)

    fs.writeFileSync(__dirname + "/../.credentials", `${name} <${email}>`)
  }

  const credentials = files.read(__dirname + "/../.credentials")

  /*
   * Declare globals
   */

  const _tree = {
      struct: [],
      file: "",
      hash: "",
    },
    branch = files.read(".mvcs/config"),
    _commit = {
      hash: "",
      parent: "",
      name: "A commit.",
      description: "",
    }
  let ignore = []

  /*
   * Command handler
   */

  switch (args[0]) {
    case "init":
      let path
      if (args.length === 1) path = ""
      else path = args[1] + "\\"

      if (path !== "" && !fs.existsSync(path)) {
        console.log("Path does not exist.")
        break
      } else if (!fs.lstatSync(path || cwd).isDirectory()) {
        console.log("Path is not a directory.")
        break
      }

      files.dir(path + ".mvcs")
      if (process.platform === "win32") cp.execSync(`attrib ${path}.mvcs +h`)
      files.dir(path + ".mvcs/objects")
      files.dir(path + ".mvcs/objects/trees")
      files.dir(path + ".mvcs/objects/commits")
      files.dir(path + ".mvcs/objects/files")
      files.dir(path + ".mvcs/branches")
      files.make(path + ".mvcs/config", "master")
      files.make(path + ".mvcs/branches/master", "null")

      console.log(`Initialized new repository at ${cwd}\\${path}`)
      break

    case "commit":
      {
        if (!isRepo()) break

        loadGlobals()

        let fileList = list(cwd, cwd).filter(item => !ignore.includes(item))

        generateTree(fileList)

        log("Enumerating changes: ")
        const changes = getChanges(_commit.parent, _tree.struct)

        if (changes.length === 0) {
          console.log(
            E.ERASE.CURRENT_LINE +
              E.CURSOR.TO_COLUMN(0) +
              "No changes to commit."
          )
          return
        }
        console.log(changes.length + ", done.")

        _tree.struct.forEach(
          item =>
            (_tree.file +=
              (_tree.file === "" ? "" : "\n") + `${item.name} ${item.hash}`)
        )

        log("Writing tree")
        _tree.hash = SHA_hash(_tree.file)
        fs.writeFileSync(`.mvcs/objects/trees/${_tree.hash}`, _tree.file)
        console.log(`, done [${_tree.hash.slice(0, 8)}].`)

        log("Constructing commit")
        _commit.new = `${_tree.hash}\n${
          _commit.hash
        }\n${new Date().toString()}\n${_commit.name}\n${
          _commit.description
        }\n${credentials}`
        _commit.hash = SHA_hash(_commit.new)
        console.log(", done.")

        log("Writing commit")
        fs.writeFileSync(`.mvcs/objects/commits/${_commit.hash}`, _commit.new)
        console.log(", done.")
        updateBranch(_commit.hash)

        /*Writing Files*/ {
          log("Writing objects: " + E.CURSOR.SAVE)
          let length = changes.length,
            count = 1
          changes.forEach(item => {
            log(
              `${
                E.CURSOR.RESTORE + E.ERASE.END_FROM_CURSOR + E.CURSOR.RESTORE
              }${Math.floor((count / length) * 100)}% (${count}/${length})`
            )
            const file = fs.readFileSync(item)
            fs.writeFileSync(`.mvcs/objects/files/${SHA_hash(file)}`, file)
            count++
          })
        }
        console.log(", done.")
        console.log(
          `\n[${branch} ${_commit.hash.slice(0, 8)}] ${_commit.name}\n  ${
            changes.length
          } files changed.`
        )
      }
      break
    case "release":
      break
    case "last":
      {
        if (!isRepo()) break

        const branch = files.read(`.mvcs/config`),
          hash = files.read(`.mvcs/branches/${branch}`)
        commit = parse.commit(files.read(`.mvcs/objects/commits/${hash}`))
        console.log(
          `${E.COLOR.TEXT.YELLOW}commit ${hash} [${E.COLOR.TEXT.GREEN}${branch}${E.COLOR.TEXT.YELLOW}]${E.COLOR.TEXT.RESET}\nAuthor: ${commit.author}\nDate:   ${commit.date}\n\n  ${commit.name}\n  ${commit.description}`
        )
      }
      break
    case "checkout":
      {
        if (!isRepo()) break

        if (args.length !== 2) {
          console.log(`Expected 1 arg(s), received ${args.length - 1}.`)
          break
        }

        if (!is(args[1], /[a-f0-9]{64}/)) {
          console.log("Argument is not a valid sha256 hash.")
          break
        }

        if (!fs.existsSync(`.mvcs/objects/commits/${args[1]}`)) {
          console.log("Argument is not an existing commit.")
          break
        }

        loadGlobals()

        if (_commit.hash === args[1]) {
          console.log(`Already at commit ${args[1]}`)
          break
        }

        let fileList = list(cwd, cwd).filter(item => !ignore.includes(item))

        generateTree(fileList)

        log("Enumerating changes: ")
        const changes = getChanges(_commit.parent, _tree.struct)
        console.log(`${changes.length}, done.`)

        if (changes.length !== 0) {
          console.log("Please commit your changes before checking-out.")
          break
        }

        const tree = parse.tree(
          files.read(
            `.mvcs/objects/trees/${
              parse.commit(files.read(`.mvcs/objects/commits/${args[1]}`)).tree
            }`
          )
        )

        updateBranch(args[1])

        {
          const dir = fs.readdirSync(cwd).filter(item => !(item === ".mvcs"))
          const length = dir.length
          let count = 1
          console.log(`Deleting files: ${E.CURSOR.SAVE}`)
          dir.forEach(item => {
            if (!(item === ".mvcs")) {
              log(
                `${
                  E.CURSOR.RESTORE + E.ERASE.END_FROM_CURSOR + E.CURSOR.RESTORE
                }${Math.floor((count / length) * 100)}% (${count}/${length})`
              )
              if (fs.lstatSync(item).isDirectory()) {
                fs.rmdirSync(item, { recursive: true })
              } else {
                fs.unlinkSync(item)
              }
              count++
            }
          })
          console.log(", done.")
        }

        {
          const length = tree.length
          let count = 1
          console.log(`Creating files: ${E.CURSOR.SAVE}`)
          tree.forEach(item => {
            log(
              `${
                E.CURSOR.RESTORE + E.ERASE.END_FROM_CURSOR + E.CURSOR.RESTORE
              }${Math.floor((count / length) * 100)}% (${count}/${length})`
            )
            files.make(
              item.name,
              fs.readFileSync(`.mvcs/objects/files/${item.hash}`)
            )
            count++
          })
          console.log(", done.")
        }
      }
      break
    case "changes":
      {
        if (!isRepo()) break

        loadGlobals()

        let fileList = list(cwd, cwd).filter(item => !ignore.includes(item))

        generateTree(fileList)

        log("Enumerating changes: ")
        const changes = getChanges(_commit.parent, _tree.struct)
        console.log(`${changes.length}, done.`)

        if (changes.length === 0) {
          console.log("No changes.")
        } else {
          console.log("Changes:")
          let count = 1
          changes.forEach(item => {
            console.log(`${count}. ${item}`)
            count++
          })
        }
      }
      break
    case "log":
      {
      }
      break
    default:
      console.log("Invalid argument.")
  }
})()
