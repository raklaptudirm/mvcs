/*
 * mvcs
 * https://github.com/raklaptudirm/mvcs
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

const { commit, tree } = require("./parse.js")

const { read } = require("./files.js")

module.exports = {
  /*
   * changes(*old, *tree)
   */
  changes: function (old, tree) {
    let _commit = {},
      tree = []
    commit(
      read(`.mvcs/objects/commit${read(`.mvcs/branches/${this.branch()}`)}`),
      _commit
    )
    _commit.tree
  },

  /*
   * branch()
   * string
   */
  branch: function () {
    return read(`.mvcs/config`)
  },
}
