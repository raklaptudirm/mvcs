/*
 * mvcs
 * https://github.com/raklaptudirm/mvcs
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

"use strict"

const { read } = require("./files.js")

module.exports = {
  /*
   * break(data)
   * string
   */
  break: function (file) {
    return file.split("\n")
  },
  /*
   * tree (data, *tree)
   */
  tree: function (data, tree) {
    this.break(data).forEach(item =>
      tree.push({
        name: item.slice(0, item.length - 65),
        hash: item.slice(item.length - 64),
      })
    )
  },
  /*
   * commit(data, *commit)
   */
  commit: function (data, commit) {
    data = this.break(data)
    Object.assign(commit, {
      tree: data[0],
      parent: data[1],
      date: data[2],
      name: data[3],
      description: data[4],
      author: data[5],
    })
  },

  ignore: function (file) {
    return this.break(file).filter(item => item.trim() !== "")
  },

  args: function (args) {
    let args = args.slice(2)
    args.command = args.splice(0, 1)[0]
    args.subcommands = []
    args.flags = []

    let flag = false
    let flagVal = {
      name: "",
      args: [],
    }

    for (const i of args) {
      if (i.startsWith("-")) {
        if (flag) {
          args.flags.push(flagVal)
        }
        flagVal = {
          name: i,
          args: [],
        }
      } else {
        if (flag) {
          flagVal.args.push(i)
        } else {
          args.subcommands.push(i)
        }
      }
    }

    if (flag) {
      args.flags.push(flagVal)
    }
  },
}
