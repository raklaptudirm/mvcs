/*
 * mvcs
 * https://github.com/raklaptudirm/mvcs
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

const cwd = process.cwd()
const { readdirSync, lstatSync } = require("fs")

module.exports = {
  init: function () {},
  loadTree: function () {
    function getFiles(ignore) {
      function list(dir, main) {
        let files = []
        readdirSync(dir).forEach(file => {
          if (file === ".mvcs" && dir === main) return
          if (lstatSync(`${dir}\\${file}`).isFile()) files.push(file)
          else
            list(`${dir}\\${file}`, dir).forEach(item => {
              files.push(`${file}\\${item}`)
            })
        })
        return files
      }
      return list(cwd, cwd).filter(item => !ignore.includes(item))
    }
  },
}
