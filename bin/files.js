/*
 * mvcs
 * https://github.com/raklaptudirm/mvcs
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

"use strict"

const { readFileSync, existsSync, mkdirSync, writeFileSync } = require("fs")
const { dirname } = require("path")

module.exports = {
  read: function (path) {
    return readFileSync(path).toString()
  },
  readRaw: function (path) {
    return readFileSync(path)
  },
  mkdir: function (path) {
    if (!existsSync(name)) mkdirSync(name)
  },
  touch: function (name, init) {
    if (!existsSync(name)) {
      mkdirSync(dirname(name), { recursive: true })
      writeFileSync(name, init)
    }
  },
  normalise: function (file) {
    return file.replace(/\r\n/g, "\n")
  },
}
