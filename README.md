# NOTE: Redesign required

<p align="center">
  <img src="https://github.com/raklaptudirm/mvcs/blob/master/imgs/text-logo.png">

  <h1 align="center"> <code>mvcs</code> </h1>
</p>

A simple local Version Control System ([`VCS`](https://en.wikipedia.org/wiki/Version_control)). `mvcs` is under heavy development.

# Installation:

## Dependencies:

- [**`Node.js`**](https://nodejs.org/en/download/) - `^14.16.0`

## Clone the repository:

```bash
$ git clone https://github.com/raklaptudirm/mvcs.git
```

## Install the script:

```bash
$ cd mvcs
$ npm install --global
```

# Docs:

## `init`

Initialize a new **`mvcs`** repository.

```bash
$ mvcs init
Initialized new repository at current/path
```

## `commit`

Commit changes to the repository.

```bash
$ mvcs commit
Enumerating changes: 1, done.
Constructing tree: 100% (8/8), done.
Writing tree, done.
Constructing commit, done.
Writing commit, done.
Updating master, done.
Writing objects: 100% (1/1), done.

[master defaf7e5] A commit.
  1 files changed.
```

If there are no changes,

```
$ mvcs commit
No changes to commit.
```

## `checkout <commit>`

Change your cwc (current working commit) to **<commit>**.
  
```bash
$ mvcs checkout defaf7e5319a0b6fdff3e745024c7c7775c23a143dc5be522f5fea8b24c65d2e
```

## `last`

Get the last commit to your repository.

```
$ mvcs last
commit defaf7e5319a0b6fdff3e745024c7c7775c23a143dc5be522f5fea8b24c65d2e [master]
Author: Rak Laptudirm <raklaptudirm@mvcstest.com>
Date:   Sun Feb 01 1998 00:00:00 GMT+0000 (GMT)

  A commit.
```

# To-Do:

- [x] **`committing`**
- [x] **`init`**
- [x] **`credentials`**
- [x] **`checkout`**
- [ ] Possibly get converted to `C` (I am learning `C`)
- [ ] **`commit log`**
- [ ] **`commit name and description`**
- [ ] **`releases`**
- [ ] **`branch creation`**
